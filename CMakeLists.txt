cmake_minimum_required(VERSION 3.11)
project(hello_cmake)
set(HELLO_CMAKE_VERSION_MAJOR 1)
set(HELLO_CMAKE_VERSION_MINOR 0)
configure_file(
    "${PROJECT_SOURCE_DIR}/HelloCmakeConfig.h.in"
    "${PROJECT_BINARY_DIR}/HelloCmakeConfig.h"
)
include_directories("${PROJECT_BINARY_DIR}")

include_directories(include)
file(GLOB SOURCES "src/*.cpp")
add_executable(hello_cmake ${SOURCES})