#ifndef HELLO_CMAKE_STUDENT_H_
#define HELLO_CMAKE_STUDENT_H_

#include <string>

class Student
{
public:
    Student(std::string name);
    ~Student();
    std::string GetName();

private:
    std::string name;
};

#endif  // HELLO_CMAKE_STUDENT_H_