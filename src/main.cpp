#include <iostream>
#include "Student.h"
#include "HelloCmakeConfig.h"

int main(int argc, char **argv)
{
    std::cout << "hello_cmake, version " << HELLO_CMAKE_VERSION_MAJOR << "." << HELLO_CMAKE_VERSION_MINOR << "\n";
    std::cout << "Hello, CMAKE!\n";
    Student student("John");
    std::cout << "Here comes a student named " << student.GetName() << "!";
}