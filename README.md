# hello_cmake

A record of my tour of learning CMake.

The following tutorials are what I followed:

* [CMake Tutorial](https://cmake.org/cmake-tutorial/): the official CMake tutorial
* [Introduction to CMake by Example](http://derekmolloy.ie/hello-world-introductions-to-cmake/): a tutorial that is much easier to follow than the official one IMHO
* [CMAKE AND VISUAL STUDIO](https://cognitivewaves.wordpress.com/cmake-and-visual-studio/): finally a tutorial bringing CMake onto Windows with Visual Studio